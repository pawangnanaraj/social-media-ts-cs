import './App.css';
import Navbar from './components/layout/Navbar.components';
import React, { Fragment, useEffect } from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import { LOGOUT } from './actions/constants';
import AppRoutes from './components/routing/Routes.components';
import Landing from './components/layout/Landing.components';
import Alert from './components/layout/Alert.components';

//Redux
import { Provider } from 'react-redux';
import store from './store';
import { loadUser } from './actions/auth.actions';
import setAuthToken from './utils/setAuthToken.utils';

const App = () => {
  useEffect(() => {
    // check for token in Local Storage
    if (localStorage.token) {
      setAuthToken(localStorage.token);
    }
    store.dispatch(loadUser());

    // log user out from all tabs if they log out in one tab
    window.addEventListener('storage', () => {
      if (!localStorage.token) store.dispatch({ type: LOGOUT });
    });
  }, []); //empty [] to run only once

  return (
    <Provider store={store}>
      <BrowserRouter>
        <Fragment>
          <Navbar />
          <Alert />
          <Switch>
            <Route exact path='/' component={Landing} />
            <Route component={AppRoutes} />
          </Switch>
        </Fragment>
      </BrowserRouter>
    </Provider>
  );
};

// const AppWrapper = () => {
//   return (
//     <Provider store={store}>
//       <Router>
//         <Fragment>
//           <Navbar />
//           <App />
//         </Fragment>
//       </Router>
//     </Provider>
//   );
// };

export default App;
