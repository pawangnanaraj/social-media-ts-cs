import React, { Fragment, useEffect } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Spinner from '../layout/Spinner';
import { getProfileById } from '../../actions/profile.actions';
import ProfileTop from './ProfileTop';
import ProfileEducation from './ProfileEducation';
import { Link, useParams } from 'react-router-dom';
import ProfileAbout from './ProfileAbout';

const Profile = ({ getProfileById, profile: { profile, loading }, auth }) => {
  const { id } = useParams();
  useEffect(() => {
    getProfileById(id);
    // eslint-disable-next-line
  }, [getProfileById, id]);

  return (
    <Fragment>
      {profile === null || loading ? (
        <Spinner />
      ) : (
        <Fragment>
          <Link to='/profiles' className='btn btn-light'>
            Back to Profiles
          </Link>
          {auth.isAuthenticated &&
            auth.loading === false &&
            auth.user._id === profile.user._id && (
              <Fragment>
                <Link to='/edit-profile' className='btn btn-dark'>
                  Edit Profile
                </Link>
                <Link to='/add-education' className='btn btn-dark'>
                  <i className='fas fa-graduation-cap text-primary'></i> Add
                  Education
                </Link>
              </Fragment>
            )}
          <div className='profile-grid my-1'>
            <ProfileTop profile={profile} />
            <ProfileAbout profile={profile} />
            <div className='profile-edu bg-white p-2'>
              <h2 className='text-primary'>Education</h2>
              {profile.education.length > 0 ? (
                // <Fragment>
                //   {profile.education.map((education) => (
                //     <ProfileEducation
                //       key={education._id}
                //       education={education}
                //     />
                //   ))}
                // </Fragment>
                <ProfileEducation education={profile.education} />
              ) : (
                <h4>No education details</h4>
              )}
            </div>
          </div>
        </Fragment>
      )}
    </Fragment>
  );
};

Profile.propTypes = {
  getProfileById: PropTypes.func.isRequired,
  profile: PropTypes.object.isRequired,
  auth: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  profile: state.profile,
  auth: state.auth,
});
export default connect(mapStateToProps, { getProfileById })(Profile);
