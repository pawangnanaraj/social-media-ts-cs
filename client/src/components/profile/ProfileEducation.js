// import React from 'react';
// import PropTypes from 'prop-types';
// import Moment from 'react-moment';

// const ProfileEducation = ({
//   education: { school, fieldofstudy, degree, to, from },
// }) => (
//   <div>
//     <h3 className='text-dark'>{school}</h3>
//     <p>
//       <Moment format='MMM YYYY'>{from}</Moment> -{' '}
//       {!to ? 'Current' : <Moment format='MMM YYYY'>{to}</Moment>}
//     </p>
//     <p>
//       <strong>Degree: </strong> {degree}
//     </p>
//     <p>
//       <strong>Specialization: </strong> {fieldofstudy}
//     </p>
//   </div>
// );

// ProfileEducation.propTypes = {
//   education: PropTypes.object.isRequired,
// };

// export default ProfileEducation;

import React, { Fragment, useEffect } from 'react';
import PropTypes from 'prop-types';
import Moment from 'react-moment';
import { useParams } from 'react-router-dom';
import { connect } from 'react-redux';
import { deleteEducation } from '../../actions/profile.actions';
import { getProfileById } from '../../actions/profile.actions';

const Education = ({
  getProfileById,
  auth,
  profile: { profile, loading },
  education,
  deleteEducation,
}) => {
  const { id } = useParams();
  useEffect(() => {
    getProfileById(id);
    // eslint-disable-next-line
  }, [getProfileById, id]);

  const displayEducation = education.map((edu) => (
    <tr key={edu._id}>
      <td>{edu.school}</td>
      <td>{edu.degree}</td>
      <td>{edu.fieldofstudy}</td>
      <td>
        <Moment format='MMM YYYY'>{edu.from}</Moment>
      </td>
      <td>
        {edu.to === null ? (
          'Current'
        ) : (
          <Moment format='MMM YYYY'>{edu.to}</Moment>
        )}
      </td>
      <td>
        {auth.isAuthenticated &&
          auth.loading === false &&
          auth.user._id === profile.user._id && (
            <button
              onClick={() => {
                deleteEducation(edu._id);
              }}
              className='btn btn-danger'
            >
              <i className='fas fa-trash' />
            </button>
          )}
      </td>
    </tr>
  ));
  return (
    <Fragment>
      <table className='table'>
        <thead>
          <tr>
            <th>School</th>
            <th className='hide-sm'>Degree</th>
            <th className='hide-sm'>Specialization</th>
            <th className='hide-sm'>From</th>
            <th className='hide-sm'>To</th>
            <th />
          </tr>
        </thead>
        <tbody>{displayEducation}</tbody>
      </table>
    </Fragment>
  );
};

Education.propTypes = {
  getProfileById: PropTypes.func.isRequired,
  education: PropTypes.array.isRequired,
  deleteEducation: PropTypes.func.isRequired,
};

const mapStateToProps = (state) => ({
  profile: state.profile,
  auth: state.auth,
});

export default connect(mapStateToProps, { deleteEducation, getProfileById })(
  Education
);
