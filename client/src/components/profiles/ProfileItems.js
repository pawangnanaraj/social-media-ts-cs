import React from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';

const ProfileItems = ({
  profile: {
    user: { _id, name },
    status,
    location,
    hobbies,
    bio,
  },
}) => {
  return (
    <div className='profile bg-light'>
      <img
        src={'https://randomuser.me/api/portraits/lego/5.jpg'}
        alt=''
        className='round-img'
      />
      <div>
        <h2>{name}</h2>
        <p>
          {status}
          {location && <span> from {location}</span>}
        </p>
        <Link to={`/profile/${_id}`} className='btn btn-primary'>
          View Profile
        </Link>
      </div>
      <ul>
        {hobbies.slice(0, 4).map((hobby, index) => (
          <li key={index} className='text-primary'>
            <i className='fas  fa-arrow-right' /> {hobby}
          </li>
        ))}
      </ul>
    </div>
  );
};

ProfileItems.propTypes = {
  profile: PropTypes.object.isRequired,
};

export default ProfileItems;
