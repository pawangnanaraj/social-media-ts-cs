import React, { Fragment, useEffect } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { getPosts } from '../../actions/post.actions';
import PostForm from './PostForm';
import PostItem from './PostItem';
import Spinner from '../layout/Spinner';
import { getCurrentProfile } from '../../actions/profile.actions';
// import { Redirect } from 'react-router';

const Posts = ({ getPosts, getCurrentProfile, post: { posts, loading } }) => {
  useEffect(() => {
    getPosts();
    getCurrentProfile();
    // eslint-disable-next-line
  }, [getPosts, getCurrentProfile]);

  //   return (
  return loading ? (
    <Spinner />
  ) : (
    <Fragment>
      <h1 className='large text-primary'>Posts</h1>
      <p className='lead'>
        <i className='fas fa-user' /> Interact with the community
      </p>
      <PostForm />
      <div className='posts'>
        {posts.map((post) => (
          <PostItem key={post._id} post={post} />
        ))}
      </div>
    </Fragment>
  );
};
Posts.propTypes = {
  post: PropTypes.object.isRequired,
  auth: PropTypes.object.isRequired,
  getPosts: PropTypes.func.isRequired,
  getCurrentProfile: PropTypes.func.isRequired,
};

const mapStateToProps = (state) => ({
  post: state.post,
  auth: state.auth,
});

export default connect(mapStateToProps, { getPosts, getCurrentProfile })(Posts);
