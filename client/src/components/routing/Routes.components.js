import React from 'react';
import { Route, Switch } from 'react-router-dom';
import '../../App.css';
import Login from '../auth/Login.components';
import Register from '../auth/Register.components';
import Dashboard from '../dashboard/Dashboard.components';
import PrivateRoutes from './PrivateRoutes.components';
import CreateProfile from '../profile-forms/CreateProfile';
import EditProfile from '../profile-forms/EditProfile';
import AddEducation from '../profile-forms/AddEducation';
import Profile from '../profile/Profile';
import Profiles from '../profiles/Profiles';
import Posts from '../posts/Posts';
import Post from '../post/Post';
const AppRoutes = () => {
  // let routes = useRoutes([
  //   { path: '/', element: <Landing /> },
  //   { path: '/register', element: <Register /> },
  //   { path: '/login', element: <Login /> },
  //   { path: '/dashboard', element: <Dashboard /> },

  //   //...
  // ]);
  // return routes;
  return (
    <section className='container'>
      <Switch>
        <Route exact path='/register' component={Register} />
        <Route exact path='/profiles' component={Profiles} />
        <Route exact path='/profile/:id' component={Profile} />
        <Route exact path='/login' component={Login} />
        <PrivateRoutes exact path='/dashboard' component={Dashboard} />
        <PrivateRoutes exact path='/create-profile' component={CreateProfile} />
        <PrivateRoutes exact path='/edit-profile' component={EditProfile} />
        <PrivateRoutes exact path='/add-education' component={AddEducation} />
        <PrivateRoutes exact path='/post' component={Posts} />
        <PrivateRoutes exact path='/post/:id' component={Post} />
      </Switch>
    </section>
  );
};

export default AppRoutes;
