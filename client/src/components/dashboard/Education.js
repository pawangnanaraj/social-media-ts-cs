import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import Moment from 'react-moment';
import { connect } from 'react-redux';
import { deleteEducation } from '../../actions/profile.actions';

const Education = ({ education, deleteEducation }) => {
  const displayEducation = education.map((edu) => (
    <tr key={edu._id}>
      <td>{edu.school}</td>
      <td>{edu.degree}</td>
      <td>{edu.fieldofstudy}</td>
      <td>
        <Moment format='MMM YYYY'>{edu.from}</Moment>
      </td>
      <td>
        {edu.to === null ? (
          'Current'
        ) : (
          <Moment format='MMM YYYY'>{edu.to}</Moment>
        )}
      </td>
      <td>
        <button
          onClick={() => {
            deleteEducation(edu._id);
          }}
          className='btn btn-danger'
        >
          <i className='fas fa-trash' />
        </button>
      </td>
    </tr>
  ));
  return (
    <Fragment>
      <h2 className='my-2'>Educational Qualifications</h2>
      <table className='table'>
        <thead>
          <tr>
            <th>School</th>
            <th className='hide-sm'>Degree</th>
            <th className='hide-sm'>Specialization</th>
            <th className='hide-sm'>From</th>
            <th className='hide-sm'>To</th>
            <th />
          </tr>
        </thead>
        <tbody>{displayEducation}</tbody>
      </table>
    </Fragment>
  );
};

Education.propTypes = {
  education: PropTypes.array.isRequired,
  deleteEducation: PropTypes.func.isRequired,
};

export default connect(null, { deleteEducation })(Education);
