import api from '../utils/api.utils';
import { setAlert } from './alert.actions';
import {
  GET_PROFILE,
  PROFILE_ERROR,
  UPDATE_PROFILE,
  CLEAR_PROFILE,
  DELETE_ACCOUNT,
  GET_PROFILES,
} from './constants';

// ---------------Get Current User Profile ----------------
export const getCurrentProfile = () => async (dispatch) => {
  try {
    const res = await api.get('profile/me');

    dispatch({
      type: GET_PROFILE,
      payload: res.data,
    });
  } catch (error) {
    const errors = error.response.data.errors;

    if (errors) {
      errors.forEach((error) => dispatch(setAlert(error.msg, 'danger')));
    }

    dispatch({
      type: PROFILE_ERROR,
      payload: {
        error: errors,
        status: error.response.status,
      },
    });
  }
};

// ---------------- Get all profiles -----------------------
export const getProfiles = () => async (dispatch) => {
  try {
    dispatch({ type: CLEAR_PROFILE });
    const res = await api.get('/profile');

    dispatch({
      type: GET_PROFILES,
      payload: res.data,
    });
  } catch (error) {
    const errors = error.response.data.errors;

    if (errors) {
      errors.forEach((error) => dispatch(setAlert(error.msg, 'danger')));
    }

    dispatch({
      type: PROFILE_ERROR,
      payload: {
        error: errors,
        status: error.response.status,
      },
    });
  }
};

// ---------------- Get profile by id -----------------------
export const getProfileById = (userId) => async (dispatch) => {
  try {
    const res = await api.get(`/profile/user/${userId}`);

    dispatch({
      type: GET_PROFILE,
      payload: res.data,
    });
  } catch (error) {
    const errors = error.response.data.errors;

    if (errors) {
      errors.forEach((error) => dispatch(setAlert(error.msg, 'danger')));
    }
    dispatch({
      type: PROFILE_ERROR,
      payload: {
        error: errors,
        status: error.response.status,
      },
    });
  }
};

//---------------Create or update a profile ---------------------
export const createProfile =
  (formData, history, edit = false) =>
  async (dispatch) => {
    try {
      const config = {
        headers: {
          'Content-Type': 'application/json',
        },
      };
      const res = await api.post('/profile', formData, config);

      dispatch({
        type: GET_PROFILE,
        payload: res.data,
      });

      dispatch(
        setAlert(edit ? 'Profile Updated' : 'Profile Created', 'success')
      );

      history.push('/dashboard');
    } catch (error) {
      const errors = error.response.data.errors;

      if (errors) {
        errors.forEach((error) => dispatch(setAlert(error.msg, 'danger')));
      }

      dispatch({
        type: PROFILE_ERROR,
        payload: {
          error: errors,
          status: error.response.status,
        },
      });
    }
  };

//--------------ADD EDUCATION ----------------------
export const addEducation = (formData, history) => async (dispatch) => {
  try {
    const config = {
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const res = await api.put('/profile/education', formData, config);

    dispatch({
      type: UPDATE_PROFILE,
      payload: res.data,
    });

    dispatch(setAlert('Education Updated', 'success'));

    history.push('/dashboard');
  } catch (error) {
    const errors = error.response.data.errors;

    if (errors) {
      errors.forEach((error) => dispatch(setAlert(error.msg, 'danger')));
    }

    dispatch({
      type: PROFILE_ERROR,
      payload: {
        error: errors,
        status: error.response.status,
      },
    });
  }
};

//---------------Delete Education --------------------
export const deleteEducation = (id) => async (dispatch) => {
  if (window.confirm('Are your sure? This can NOT be undone!')) {
    try {
      const res = await api.delete(`/profile/education/${id}`);

      dispatch({
        type: UPDATE_PROFILE,
        payload: res.data,
      });

      dispatch(setAlert('Education deleted', 'success'));
    } catch (error) {
      const errors = error.response.data.errors;

      if (errors) {
        errors.forEach((error) => dispatch(setAlert(error.msg, 'danger')));
      }
      dispatch({
        type: PROFILE_ERROR,
        payload: {
          error: errors,
          status: error.response.status,
        },
      });
    }
  }
};

//--------------------Delete Account ---------------------
export const deleteAccount = () => async (dispatch) => {
  if (window.confirm('Are your sure? This can NOT be undone!')) {
    try {
      await api.delete('/profile');

      dispatch({
        type: CLEAR_PROFILE,
      });
      // dispatch({
      //   type: CLEAR_USER,
      // });
      dispatch({
        type: DELETE_ACCOUNT,
      });

      dispatch(setAlert('Your account has been permanantly deleted'));
    } catch (error) {
      const errors = error.response.data.errors;

      if (errors) {
        errors.forEach((error) => dispatch(setAlert(error.msg, 'danger')));
      }
      dispatch({
        type: PROFILE_ERROR,
        payload: {
          error: errors,
          status: error.response.status,
        },
      });
    }
  }
};
