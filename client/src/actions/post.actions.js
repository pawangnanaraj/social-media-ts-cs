import api from '../utils/api.utils';
import { setAlert } from './alert.actions';
import {
  GET_POSTS,
  UPDATE_LIKES,
  POST_ERROR,
  DELETE_POST,
  ADD_POST,
  GET_POST,
  ADD_COMMENT,
  REMOVE_COMMENT,
} from './constants';

//---------------Get Posts ---------------------
export const getPosts = () => async (dispatch) => {
  try {
    const res = await api.get('/post');

    dispatch({
      type: GET_POSTS,
      payload: res.data,
    });
  } catch (error) {
    const errors = error.response.data.errors;

    if (errors) {
      errors.forEach((error) => dispatch(setAlert(error.msg, 'danger')));
    }

    dispatch({
      type: POST_ERROR,
      payload: {
        error: error.response.data.errors,
        status: error.response.status,
      },
    });
  }
};

//----------------Liking a Post ----------------
export const likePosts = (id) => async (dispatch) => {
  try {
    const res = await api.put(`/post/like/${id}`);

    dispatch({
      type: UPDATE_LIKES,
      payload: { id, likes: res.data },
    });
  } catch (error) {
    const errors = error.response.data.errors;

    if (errors) {
      errors.forEach((error) => dispatch(setAlert(error.msg, 'danger')));
    }

    dispatch({
      type: POST_ERROR,
      payload: {
        error: error.response.data.errors,
        status: error.response.status,
      },
    });
  }
};

//---------------- Un Liking a Post ----------------
export const unLikePosts = (id) => async (dispatch) => {
  try {
    const res = await api.put(`/post/unlike/${id}/`);

    dispatch({
      type: UPDATE_LIKES,
      payload: { id, likes: res.data },
    });
  } catch (error) {
    const errors = error.response.data.errors;

    if (errors) {
      errors.forEach((error) => dispatch(setAlert(error.msg, 'danger')));
    }

    dispatch({
      type: POST_ERROR,
      payload: {
        error: errors,
        status: error.response.status,
      },
    });
  }
};

// Delete post
export const deletePost = (id) => async (dispatch) => {
  try {
    await api.delete(`/post/${id}`);

    dispatch({
      type: DELETE_POST,
      payload: id,
    });

    dispatch(setAlert('Post Removed', 'success'));
  } catch (error) {
    const errors = error.response.data.errors;

    if (errors) {
      errors.forEach((error) => dispatch(setAlert(error.msg, 'danger')));
    }

    dispatch({
      type: POST_ERROR,
      payload: {
        error: errors,
        status: error.response.status,
      },
    });
  }
};

// Add post
export const addPost = (formData) => async (dispatch) => {
  try {
    const res = await api.post('/post', formData);

    dispatch({
      type: ADD_POST,
      payload: res.data,
    });

    dispatch(setAlert('Post Created', 'success'));
  } catch (error) {
    const errors = error.response.data.errors;

    if (errors) {
      errors.forEach((error) => dispatch(setAlert(error.msg, 'danger')));
    }

    dispatch({
      type: POST_ERROR,
      payload: {
        error: errors,
        status: error.response.status,
      },
    });
  }
};

//--------------------- Get a single post ------------------------------------
export const getPost = (id) => async (dispatch) => {
  try {
    const res = await api.get(`/post/${id}`);

    dispatch({
      type: GET_POST,
      payload: res.data,
    });
  } catch (error) {
    const errors = error.response.data.errors;

    if (errors) {
      errors.forEach((error) => dispatch(setAlert(error.msg, 'danger')));
    }

    dispatch({
      type: POST_ERROR,
      payload: {
        error: errors,
        status: error.response.status,
      },
    });
  }
};

// ------------------------ Add Comment ---------------------------------------
export const addComment = (postId, formData) => async (dispatch) => {
  try {
    const res = await api.post(`/post/${postId}`, formData);

    dispatch({
      type: ADD_COMMENT,
      payload: res.data,
    });

    dispatch(setAlert('Comment Added', 'success'));
  } catch (error) {
    const errors = error.response.data.errors;

    if (errors) {
      errors.forEach((error) => dispatch(setAlert(error.msg, 'danger')));
    }

    dispatch({
      type: POST_ERROR,
      payload: {
        error: errors,
        status: error.response.status,
      },
    });
  }
};

// ------------------------ Delete Comment ---------------------------------------
export const deleteComment = (postId, commentId) => async (dispatch) => {
  try {
    await api.delete(`/post/${postId}/${commentId}`);

    dispatch({
      type: REMOVE_COMMENT,
      payload: commentId,
    });

    dispatch(setAlert('Comment Removed', 'success'));
  } catch (error) {
    const errors = error.response.data.errors;

    if (errors) {
      errors.forEach((error) => dispatch(setAlert(error.msg, 'danger')));
    }

    dispatch({
      type: POST_ERROR,
      payload: {
        error: errors,
        status: error.response.status,
      },
    });
  }
};
