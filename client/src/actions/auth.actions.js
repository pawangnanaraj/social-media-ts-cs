import api from '../utils/api.utils';
import { setAlert } from './alert.actions'; //to show errors

import {
  REGISTER_SUCCESS,
  REGISTER_FAIL,
  LOGIN_SUCCESS,
  LOGIN_FAIL,
  LOGOUT,
  USER_LOADED,
  AUTH_ERROR,
  CLEAR_PROFILE,
} from './constants';

import setAuthToken from '../utils/setAuthToken.utils';

//---------------------------------LOADING USER -----------------------------------

export const loadUser = () => async (dispatch) => {
  if (localStorage.token) {
    // console.log(`local: ${localStorage.token}`);
    setAuthToken(localStorage.token);
  }
  try {
    const res = await api.get('/auth');

    dispatch({
      type: USER_LOADED,
      payload: res.data,
    });
  } catch (error) {
    dispatch({
      type: AUTH_ERROR,
      payload: {
        error: error.response.data.errors,
        status: error.response.status,
      },
    });
  }
};

//----------------------------------- register User ---------------------------------
export const register =
  ({ name, email, password }) =>
  async (dispatch) => {
    const body = JSON.stringify({ name, email, password });
    try {
      const res = await api.post('/users', body);

      dispatch({
        type: REGISTER_SUCCESS,
        payload: res.data,
      });

      dispatch(loadUser());
    } catch (error) {
      const errors = error.response.data.errors;

      if (errors) {
        errors.forEach((error) => dispatch(setAlert(error.msg, 'danger')));
      }

      dispatch({
        type: REGISTER_FAIL,
        payload: {
          error: error.response.data.errors,
          status: error.response.status,
        },
      });
    }
  };

//----------------------------------- Login User ---------------------------------
export const login = (email, password) => async (dispatch) => {
  const body = { email, password };
  try {
    const res = await api.post('/auth', body);

    dispatch({
      type: LOGIN_SUCCESS,
      payload: res.data,
    });

    dispatch(loadUser());
  } catch (error) {
    const errors = error.response.data.errors;
    console.log(error.response.data.errors);
    dispatch({
      type: LOGIN_FAIL,
      payload: {
        error: error.response.data.errors,
        status: error.response.status,
      },
    });

    // const errors = error.response.data;

    if (errors) {
      errors.forEach((error) => dispatch(setAlert(error.msg, 'danger')));
    }
  }
};

//------------------------------------log out user -----------------------------------
export const logout = () => (dispatch) => {
  dispatch({ type: CLEAR_PROFILE });
  dispatch({ type: LOGOUT });
};
