// this file is root reducer

import { combineReducers } from 'redux';
import alert from './alert.reducers';
import auth from './auth.reducers';
import profile from './profile.reducers';
import post from './post.reducer';

export default combineReducers({ alert, auth, profile, post });
