import express, { Application, Request, Response } from 'express';
import { connectDB } from './helpers/connectMongoDB';
import { authRouter } from './router/api/auth.router';
import { registerUserRouter } from './router/api/user.router';
import { postRouter } from './router/api/post.router';
import { profileRouter } from './router/api/profile.router';
import path from 'path';

import cors from 'cors';
//initialise our app
const app: Application = express();

//connect to DB
connectDB();
app.use(cors());
app.use(express.json());
// app.use(express.urlencoded());

console.log(process.env.NODE_ENV);

// app.get('', (req: Request, res: Response) => res.send('API is working'));

app.use('/api/auth', authRouter);
app.use('/api/users', registerUserRouter);
//post APIs
app.use('/api/post', postRouter);
app.use('/api/profile', profileRouter);

//Serve static assests in production
if (process.env.NODE_ENV === 'production') {
  //Set static folder

  app.use(express.static('client/build'));

  app.get('*', (req: Request, res: Response) => {
    res.sendFile(path.resolve('/app/client/build/index.html'));
  });
}

const PORT = process.env.PORT || 5000;

app.listen(PORT, () => console.log(`server running ${PORT} `));

export default app;
