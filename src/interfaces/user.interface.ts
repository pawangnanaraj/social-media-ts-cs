export default interface User {
  name: string;
  email: string;
  password: string;
  id?: string;
  //date: number;
}
