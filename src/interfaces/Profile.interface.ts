import { Schema } from 'mongoose';

export default interface Profile {
  user: Schema.Types.ObjectId | string;
  location: string;
  status: string;
  hobbies: [string];
  bio: string;
  social: {
    facebook: string;
    linkedin: string;
    instagram: string;
  };
  education: [
    {
      school: string;
      degree: string;
      fieldofstudy: string;
      from: Date;
      to: Date;
      current: Boolean;
      description: string;
      id?: string;
    }
  ];
}
