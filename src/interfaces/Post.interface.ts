import { Schema } from 'mongoose';

// 1. Create an interface representing a Post in MongoDB.
export default interface Post {
  user: Schema.Types.ObjectId;
  text: string;
  name: string;
  likes: [{ user: Schema.Types.ObjectId }];
  comments: [
    {
      user: Schema.Types.ObjectId;
      text: string;
      name: string;
      id?: string;
    }
  ];
}

// 1. Create an interface representing a Post in MongoDB.
export default interface Comment {
  user: Schema.Types.ObjectId;
  text: string;
  name: string;
  id?: string;
}
