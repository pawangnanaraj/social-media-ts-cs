export default interface newEdu {
  school: string;
  degree: string;
  fieldofstudy: string;
  from: Date;
  to: Date;
  current: Boolean;
  description: string;
  id?: string;
}
