import { Schema, model } from 'mongoose';
import User from '../interfaces/user.interface';
// 1. Create an interface representing a User in MongoDB.
// interface User {
//   name: string;
//   email: string;
//   password: string;
//   id?: string;
//   //date: number;
// }

// 2. Create a Schema corresponding to the User interface.
const UserSchema = new Schema<User>(
  {
    name: { type: String, required: true, unique: true },
    email: { type: String, required: true, unique: true },
    password: { type: String, required: true },
  },
  { timestamps: true }
  //date: { type: Date, default(): Date.now; },
);

// 3. Create a Model.
export const UserModel = model<User>('User', UserSchema);

//module.exports = mongoose.model('user', UserSchema);
