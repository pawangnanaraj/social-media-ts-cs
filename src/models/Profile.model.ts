import { Schema, model } from 'mongoose';
import Profile from '../interfaces/Profile.interface';

// 1. Create an interface representing a Profile in MongoDB.

// 2. Create a Schema corresponding to the Profile interface.
const ProfileSchema = new Schema<Profile>(
  {
    user: {
      type: Schema.Types.ObjectId,
      ref: 'User',
    },
    location: {
      type: String,
    },
    status: {
      type: String,
      required: true,
    },
    hobbies: {
      type: [String],
    },
    bio: {
      type: String,
    },
    social: {
      facebook: {
        type: String,
      },
      linkedin: {
        type: String,
      },
      instagram: {
        type: String,
      },
    },
    education: [
      {
        school: {
          type: String,
          required: true,
        },
        degree: {
          type: String,
          required: true,
        },
        fieldofstudy: {
          type: String,
          required: true,
        },
        from: {
          type: Date,
          required: true,
        },
        to: {
          type: Date,
        },
        current: {
          type: Boolean,
          default: false,
        },
        description: {
          type: String,
        },
      },
    ],
  },
  { timestamps: true }
);

// 3. Create a Model.
export const ProfileModel = model<Profile>('Profiles', ProfileSchema);
