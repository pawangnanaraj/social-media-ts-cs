import { Request, Response } from 'express';
import { validationResult } from 'express-validator';
import userService from '../services/user.service';

const registerUser = async (req: Request, res: Response) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(400).json({ errors: errors.array() });
  }

  const { name, email, password } = req.body;
  try {
    let token = await userService.registerUser(name, email, password);

    res.json({ token: token });
  } catch (error: any) {
    // console.error(error.message, error.status);
    return res.status(error.status).json({ errors: [{ msg: error.message }] });
  }
};

export default {
  registerUser,
};
