import { Request, Response } from 'express';
import { validationResult } from 'express-validator';
import authService from '../services/auth.service';

//----------------------------SignIn Controller------------------------------
const signIn = async (req: Request, res: Response) => {
  try {
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }

    //see if the user exits, send an error
    const { email, password } = req.body;
    const token = await authService.signIn(email, password);
    res.json({ token: token });
  } catch (error: any) {
    // console.error(error.message, error.status);
    return res.status(error.status).json({ errors: [{ msg: error.message }] });
  }
};

//----------------------------Authenticate Controller------------------------

const authenticate = async (req: Request, res: Response) => {
  try {
    //get only id and leave the password
    const id: string = req.body.user.user.id;
    // console.log(`_id: ${id}`);

    const userId = await authService.authenticate(id);
    // console.log(`userId: ${userId}`);

    res.json(userId);
  } catch (error: any) {
    // console.error(error.message, error.status);
    return res.status(error.status).json({ errors: [{ msg: error.message }] });
  }
};

//----------------------------Export Auth Controller--------------------------
export default {
  signIn,
  authenticate,
};
