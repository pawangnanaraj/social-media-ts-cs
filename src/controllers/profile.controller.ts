import express, { Request, Response } from 'express';
import { validationResult } from 'express-validator';
import Profile from '../interfaces/Profile.interface';
import profileService from '../services/profile.service';

export const profileRouter = express.Router();

//-------------------------------Get all profiles Controller---------------------------
const getProfiles = async (req: Request, res: Response) => {
  try {
    const profiles = await profileService.getProfiles();
    res.json(profiles);
  } catch (error: any) {
    // console.error(error.message, error.status);
    res.status(500).json('Server Error');
  }
};

//-----------------------Create or update user profile Controller---------------------------

const createProfile = async (req: Request, res: Response) => {
  const validationErrors = validationResult(req);
  if (!validationErrors.isEmpty()) {
    return res.status(400).json({ errors: validationErrors.array() });
  }

  // destructure the request
  const {
    location,
    status,
    bio,
    hobbies,
    facebook,
    linkedin,
    instagram,

    ...rest
  } = req.body;
  console.log(`Facebook: ${facebook}`);
  // build a profile
  const profileFields = {} as Profile;
  profileFields.user = req.body.user.user.id;
  if (location) profileFields.location = location;
  if (status) profileFields.status = status;
  if (bio) profileFields.bio = bio;
  if (hobbies) {
    profileFields.hobbies = hobbies
      .split(',')
      .map((hobby: string) => hobby.trim());
  }

  // Build socialFields object
  const socialFields = { instagram, linkedin, facebook };
  // add to profileFields
  profileFields.social = socialFields;
  const userId = req.body.user.user.id;

  try {
    // Using upsert option (creates new doc if no match is found):
    const profile = await profileService.createProfile(userId, profileFields);
    res.json(profile);
  } catch (error: any) {
    // console.error(error.message, error.status);
    return res.status(error.status).json({ errors: [{ msg: error.message }] });
  }
};

//---------------------------Get current users profile Controller---------------------------

const currentProfile = async (req: Request, res: Response) => {
  try {
    const userId = req.body.user.user.id;
    const profile = await profileService.currentProfile(userId);
    res.json(profile);
  } catch (error: any) {
    // console.error(error.message, error.status);
    return res.status(error.status).json({ errors: [{ msg: error.message }] });
  }
};

//---------------------------Get profile by id Controller---------------------------
const getProfileById = async (req: Request, res: Response) => {
  try {
    const userId = req.params.user_id;
    const profile = await profileService.getProfileById(userId);

    res.json(profile);
  } catch (error: any) {
    // console.error(error.message, error.status);
    // if (error.kind == 'ObjectId') {
    //   res.status(400).json({ msg: 'Profile not found' });
    // }
    return res.status(error.status).json({ errors: [{ msg: error.message }] });
  }
};

//---------------------------Delete profile Controller---------------------------
const deleteProfile = async (req: Request, res: Response) => {
  try {
    // Remove user posts
    // Remove profile
    // Remove user
    const userId = req.body.user.user.id;
    await profileService.deleteProfile(userId);

    res.json({ msg: 'User deleted' });
  } catch (error: any) {
    // console.error(error.message, error.status);
    res.status(500).send('Server Error');
  }
};

//---------------------------add Education Controller---------------------------
const addEducation = async (req: Request, res: Response) => {
  const validationErrors = validationResult(req);
  if (!validationErrors.isEmpty()) {
    return res.status(400).json({ errors: validationErrors.array() });
  }

  const { school, degree, fieldofstudy, from, to, current, description } =
    req.body;

  const newEdu = {
    school,
    degree,
    fieldofstudy,
    from,
    to,
    current,
    description,
  };

  const userId = req.body.user.user.id;

  try {
    const profile = await profileService.addEducation(userId, newEdu);

    res.json(profile);
  } catch (error: any) {
    // console.error(error.message, error.status);
    return res.status(error.status).json({ errors: [{ msg: error.message }] });
  }
};

//---------------------------Delete Education Controller---------------------------
const deleteEducation = async (req: Request, res: Response) => {
  try {
    const userId = req.body.user.user.id;
    const eduId = req.params.edu_id;
    const profile = await profileService.deleteEducation(userId, eduId);
    res.json(profile);
  } catch (error: any) {
    // console.error(error.message, error.status);
    return res.status(error.status).json({ errors: [{ msg: error.message }] });
  }
};

export default {
  getProfiles,
  createProfile,
  currentProfile,
  getProfileById,
  deleteProfile,
  addEducation,
  deleteEducation,
};
