import { Request, Response } from 'express';
import { validationResult } from 'express-validator';
import postService from '../services/post.service';

//--------------------------Create Post Controller---------------------------------
const createPost = async (req: Request, res: Response) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(400).json({ errors: errors.array() });
  }
  // console.log(req.body);

  try {
    const _id: string = req.body.user.user.id;
    const postText: string = req.body.text;

    const newPost = await postService.createPost(_id, postText);

    res.json(newPost);
    // res.json('testing');
  } catch (error: any) {
    // console.error(error.message, error.status);
    return res.status(error.status).json({ errors: [{ msg: error.message }] });
  }
};

//--------------------------Edit Post Controller---------------------------------
const editPost = async (req: Request, res: Response) => {
  const validationErrors = validationResult(req);
  if (!validationErrors.isEmpty()) {
    return res.status(400).json({ errors: validationErrors.array() });
  }
  try {
    const postId = req.params.id;
    const userId: string = req.body.user.user.id;
    const postText: string = req.body.text;

    const post = await postService.editPost(userId, postId, postText);

    return res.json(post);
  } catch (error: any) {
    // console.error(error.message, error.status);
    return res.status(error.status).json({ errors: [{ msg: error.message }] });
  }
};

//--------------------------Get all Post Controller---------------------------------
const getAllPost = async (req: Request, res: Response) => {
  try {
    const posts = await postService.getAllPost();
    res.json(posts);
  } catch (error: any) {
    // console.error(error.message, error.status);
    return res.status(error.status).json({ errors: [{ msg: error.message }] });
  }
};

//--------------------------Get Post by Id Controller---------------------------------
const getPostById = async (req: Request, res: Response) => {
  try {
    const postId = req.params.id;
    const post = await postService.getPostById(postId);
    res.json(post);
  } catch (error: any) {
    // console.error(error.message, error.status);
    return res.status(error.status).json({ errors: [{ msg: error.message }] });
  }
};

//--------------------------Delete Post Controller---------------------------------
const deletePost = async (req: Request, res: Response) => {
  try {
    const postId = req.params.id;
    const userId = req.body.user.user.id;

    await postService.deletePost(postId, userId);

    res.json({ msg: 'Post removed' });
  } catch (error: any) {
    // console.error(error.message, error.status);
    return res.status(error.status).json({ errors: [{ msg: error.message }] });
  }
};

//-------------------------Create Comment Controller-------------------------------
const createComment = async (req: Request, res: Response) => {
  const validationErrors = validationResult(req);
  if (!validationErrors.isEmpty()) {
    return res.status(400).json({ errors: validationErrors.array() });
  }

  try {
    const userId = req.body.user.user.id;
    const postId = req.params.postId;
    const commentText = req.body.text;

    const post = await postService.createComment(userId, postId, commentText);

    res.json(post.comments);
  } catch (error: any) {
    // console.error(error.message, error.status);
    return res.status(error.status).json({ errors: [{ msg: error.message }] });
  }
};

//-------------------------Edit Comment Controller---------------------------------
const editComment = async (req: Request, res: Response) => {
  const validationErrors = validationResult(req);
  if (!validationErrors.isEmpty()) {
    return res.status(400).json({ errors: validationErrors.array() });
  }
  try {
    const postId = req.params.postId;
    const commentId = req.params.comment_id;
    const userId = req.body.user.user.id;
    const commentText = req.body.text;

    const comment = await postService.editComment(
      postId,
      commentId,
      userId,
      commentText
    );

    return res.json(comment);
  } catch (error: any) {
    // console.error(error.message, error.status);
    return res.status(error.status).json({ errors: [{ msg: error.message }] });
  }
};

//-------------------------Delete Comment Controller---------------------------------
const deleteComment = async (req: Request, res: Response) => {
  try {
    const postId = req.params.postId;
    let commentId = req.params.comment_id;
    const userId = req.body.user.user.id;

    await postService.deleteComment(postId, commentId, userId);

    return res.json('Ok');
  } catch (error: any) {
    // console.error(error.message, error.status);
    return res.status(error.status).json({ errors: [{ msg: error.message }] });
  }
};

//---------------------------Like Post Controller---------------------------------
const likePost = async (req: Request, res: Response) => {
  try {
    const postId = req.params.postId;
    const userId = req.body.user.user.id;
    // console.log(postId);
    const likes = await postService.likePost(postId, userId);
    return res.json(likes);
  } catch (error: any) {
    // console.error(error.message, error.status);
    return res.status(error.status).json({ errors: [{ msg: error.message }] });
  }
};

//---------------------------unlike Comment Controller-------------------------------
const unlikePost = async (req: Request, res: Response) => {
  try {
    const postId = req.params.postId;
    const userId = req.body.user.user.id;
    // const likeId = req.params.likeId;
    const postLikes = await postService.unlikePost(postId, userId);

    return res.json(postLikes);
  } catch (error: any) {
    // console.error(error.message, error.status);
    return res.status(error.status).json({ errors: [{ msg: error.message }] });
  }
};

export default {
  createPost,
  editPost,
  getAllPost,
  getPostById,
  deletePost,
  createComment,
  editComment,
  deleteComment,
  likePost,
  unlikePost,
};
