import { UserModel } from '../models/User.model';
import jwt from 'jsonwebtoken';
import bcrypt from 'bcrypt';
import config from 'config';
import { UserAlreadyAvailableError } from '../middleware/errorHandler.middleware';

const registerUser = async (name: string, email: string, password: string) => {
  let user = await UserModel.findOne({ email });

  //see if the user exits, send an error
  if (user) {
    throw new UserAlreadyAvailableError('user already exists');
  } else {
    user = new UserModel({
      name,
      email,
      password,
    });

    //encrypt the password
    const salt = await bcrypt.genSalt(10);

    user.password = await bcrypt.hash(password, salt);

    await user.save();

    //return the JWT
    const payload = {
      user: {
        id: user.id,
      },
    };
    // console.log(`payload: ${payload.user.id}`);

    const token = jwt.sign(payload, config.get('jwtSecret'), {
      expiresIn: 360000,
    });
    return token;
  }
};

export default {
  registerUser,
};
