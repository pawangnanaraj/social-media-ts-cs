import { UserModel } from '../models/User.model';
import jwt from 'jsonwebtoken';
import bcrypt from 'bcrypt';
import config from 'config';
import {
  NotFoundError,
  NotMatchError,
} from '../middleware/errorHandler.middleware';

//----------------------------SignIn Service----------------------------------
const signIn = async (email: string, password: string) => {
  //see if the user exits, send an error

  let user = await UserModel.findOne({ email });
  if (!user) {
    throw new NotMatchError('User not Found (or) Invalid emails');
  }

  //verify password
  const passwordMatch = await bcrypt.compare(password, user.password);

  if (!passwordMatch) {
    throw new NotMatchError('Invalid Password');
  }

  //return the JWT
  const payload = {
    user: {
      id: user.id,
    },
  };

  const token = jwt.sign(payload, config.get('jwtSecret'), {
    expiresIn: 360000,
  });

  return token;
};

//----------------------------Authenticate Service-----------------------------
const authenticate = async (_id: string) => {
  //get only id and leave the password
  const userId = await UserModel.findById(_id).select('-password');

  if (userId == null) {
    // console.log('not found');
    throw new NotFoundError('User not found');
  }
  // console.log('haha');
  return userId;
};

//----------------------------Export Auth Services-----------------------------
export default {
  signIn,
  authenticate,
};
