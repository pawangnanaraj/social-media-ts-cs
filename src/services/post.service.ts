import {
  NotFoundError,
  UnauthorizedUserError,
  AlreadyLikedError,
} from '../middleware/errorHandler.middleware';
import { ObjectId } from 'mongoose';
import { PostModel } from '../models/Posts.model';
import { UserModel } from '../models/User.model';

//-----------------------------Create Post Service---------------------------------
const createPost = async (_id: string, postText: string) => {
  const userDetails = await UserModel.findById(_id).select('-password');

  const newPost = new PostModel({
    text: postText,
    name: userDetails.name,
    user: _id,
  });

  await newPost.save();

  return newPost;
};

//-----------------------------Edit Post Service---------------------------------
const editPost = async (userId: string, postId: string, postText: string) => {
  const post = await PostModel.findById(postId);

  //find it the post exists
  if (!post) {
    throw new NotFoundError('Post is not available');
  }

  //check is the user is authorised
  const authorisedUser = post.user.toString();

  if (authorisedUser !== userId) {
    throw new UnauthorizedUserError('User not Authorized to edit the post');
  } else {
    //create the updated post

    const editedPost = await PostModel.findOneAndUpdate(
      { id: postId },
      { $set: { text: postText } },
      { new: true }
    );
    return editedPost;
  }
};

//-----------------------------Get all Post Service------------------------------
const getAllPost = async () => {
  const posts = await PostModel.find().sort({ createdAt: -1 });
  return posts;
};

//----------------------------Get Post by Id Service-----------------------------
const getPostById = async (postId: string) => {
  const post = await PostModel.findById(postId);
  //check if post is found
  if (!post) {
    throw new NotFoundError('Post is not available');
  }
  return post;
};

//----------------------------Delete Post Service--------------------------------
const deletePost = async (postId: string, userId: string) => {
  const post = await PostModel.findById(postId);

  if (!post) {
    throw new NotFoundError('Post is not available');
  }

  // Check user
  if (post.user.toString() !== userId) {
    throw new UnauthorizedUserError('User not Authorized to delete the post');
  }

  await post.remove();

  //res.json({ msg: 'Post removed' });
};

//---------------------------Create Comment Service------------------------------
const createComment = async (
  userId: ObjectId,
  postId: string,
  commentText: string
) => {
  const user = await UserModel.findById(userId).select('-password');

  const newComment = {
    text: commentText,
    name: user.name,
    user: userId,
  };
  //Find the post
  let post = await PostModel.findOneAndUpdate(
    { _id: postId },
    { $push: { comments: newComment } },
    { new: true }
  );
  // console.log(post);
  return post;
};

//------------------------- testing Edit Comment Service-----------------------
const editComment = async (
  postId: string,
  commentId: string,
  userId: string,
  commentText: string
) => {
  //get the comment
  const post = await PostModel.findById(postId);
  const comment = post.comments.find((comment) => comment.id === commentId);

  //find it the comment exists
  if (!comment) {
    throw new NotFoundError('Comment is not available');
  }

  //check is the user is authorised
  const authorisedUser = comment.user.toString();
  if (authorisedUser !== userId) {
    throw new UnauthorizedUserError('User is not authorized');
  }
  const editedPost = await PostModel.findOneAndUpdate(
    { id: postId, 'comments._id': commentId },
    { $set: { 'comments.$.text': commentText } },
    { new: true }
  );
  return editedPost.comments.find((comment) => comment.id === commentId);
};

//------------------------delete comment comment service ----------------------
const deleteComment = async (
  postId: string,
  commentId: string,
  userId: ObjectId
) => {
  const post = await PostModel.findById(postId);

  // Pull out comment
  const comment = post.comments.find((comment) => comment.id === commentId);
  // If comment does not exist
  if (!comment) {
    throw new NotFoundError('Comment does not exist');
  }
  // Check user
  if (comment.user.toString() !== userId.toString()) {
    throw new UnauthorizedUserError('User is not Authorised');
  }

  await PostModel.findOneAndUpdate(
    { id: postId },
    { $pull: { comments: { _id: commentId } } },
    { multi: true }
  );
};

//---------------------------Like Post Service--------------------------------
const likePost = async (postId: string, userId: ObjectId) => {
  //get the post by id
  const post = await PostModel.findById(postId);

  // Check if the post has already been liked
  if (
    post.likes.filter((like) => like.user.toString() === userId.toString())
      .length > 0
  ) {
    throw new AlreadyLikedError('Post already liked');
  }

  post.likes.unshift({ user: userId });

  await post.save();

  return post.likes;
};

//---------------------------unlike Post Service------------------------------
const unlikePost = async (postId: string, userId: ObjectId) => {
  const post = await PostModel.findById(postId);

  // Check if the post has not yet been liked by the user
  if (
    post.likes.filter((like) => like.user.toString() === userId.toString())
      .length === 0
  ) {
    throw new AlreadyLikedError("You haven't liked the post");
  }

  //console.log(post.likes);
  console.log(
    post.likes.filter((like) => like.user.toString() === userId.toString())
      .length
  );
  // await PostModel.findOneAndUpdate(
  //   { id: postId },
  //   { $pull: { likes: { _id: likeId, user: userId } } }
  //   { multi: true }
  // );
  //get the removed index
  const removeIndex = post.likes
    .map((like) => like.user.toString())
    .indexOf(userId.toString());

  //remove one like from the given index
  post.likes.splice(removeIndex, 1);

  await post.save();

  return post.likes;
};

export default {
  createPost,
  editPost,
  getAllPost,
  getPostById,
  deletePost,
  createComment,
  editComment,
  deleteComment,
  likePost,
  unlikePost,
};
