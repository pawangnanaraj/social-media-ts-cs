import express from 'express';
import { ProfileModel } from '../models/Profile.model';
import { PostModel } from '../models/Posts.model';
import { UserModel } from '../models/User.model';
import Profile from '../interfaces/Profile.interface';
import newEdu from '../interfaces/newEdu.interface';
import { ObjectId } from 'mongoose';
import { NotFoundError } from '../middleware/errorHandler.middleware';
export const profileRouter = express.Router();

//-------------------------------Get all profiles---------------------------
const getProfiles = async () => {
  const profiles = await ProfileModel.find().populate('user', 'name');
  return profiles;
};

//-----------------------Create or update user profile---------------------------

const createProfile = async (userId: ObjectId, profileFields: Profile) => {
  let profile = await ProfileModel.findOne({ user: userId });
  //update profile if found
  if (profile) {
    profile = await ProfileModel.findOneAndUpdate(
      { user: userId },
      { $set: profileFields },
      { new: true }
    );
    return profile;
  }

  //if profile not found create a profile
  profile = new ProfileModel(profileFields);

  await profile.save();
  return profile;
};

//---------------------------Get current users profile---------------------------

const currentProfile = async (userId: ObjectId) => {
  const profile = await ProfileModel.findOne({
    user: userId,
  }).populate('user', 'name');

  if (!profile) {
    throw new NotFoundError('There is no profile for this user');
  }

  return profile;
};

//---------------------------Get profile by id---------------------------
const getProfileById = async (userId: string) => {
  const profile = await ProfileModel.findOne({
    user: userId,
  }).populate('user', 'name');

  if (!profile) {
    throw new NotFoundError('There is no profile for this user');
  }

  return profile;
};

const deleteProfile = async (userId: ObjectId) => {
  // Remove user posts
  // Remove profile
  // Remove user
  await Promise.all([
    PostModel.deleteMany({ user: userId }),
    ProfileModel.findOneAndRemove({ user: userId }),
    UserModel.findOneAndRemove({ _id: userId }),
  ]);
};

const addEducation = async (userId: ObjectId, newEdu: newEdu) => {
  const profile = await ProfileModel.findOne({
    user: userId,
  });

  profile.education.unshift(newEdu);
  await profile.save();

  return profile;
};

const deleteEducation = async (userId: ObjectId, eduId: string) => {
  const profile = await ProfileModel.findOne({
    user: userId,
  });

  //remove index
  const removeIndex = profile.education.map((item) => item.id).indexOf(eduId);

  profile.education.splice(removeIndex, 1);

  await profile.save();
  return profile;
};

export default {
  getProfiles,
  createProfile,
  currentProfile,
  getProfileById,
  deleteProfile,
  addEducation,
  deleteEducation,
};
