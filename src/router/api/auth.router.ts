import express from 'express';
import { authMiddleware } from '../../middleware/auth.middleware';
import authCtrl from '../../controllers/auth.controller';
import { check } from 'express-validator';
// import { loginValidator } from '../../middleware/validation.middleware';

export const authRouter = express.Router();

// route --> get api/auth
// access --> public
//Authenticate the user and return the id
//this is a protected route
//console.log(auth);
authRouter.get('/', authMiddleware, authCtrl.authenticate);

// route --> post api/authts
// access --> public
//Authenticate the user and login
//this is a protected route
authRouter.post(
  '/',
  [
    check('email', 'Please enter a valid email').isEmail(),
    check('password', 'password id required').exists(),
  ],
  // loginValidator(),
  authCtrl.signIn
);
