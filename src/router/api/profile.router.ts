import express from 'express';
import { authMiddleware } from '../../middleware/auth.middleware';
import { check } from 'express-validator';
import profileCtrl from '../../controllers/profile.controller';

export const profileRouter = express.Router();

// @route    GET api/profile
// @desc     Get all profiles
// @access   Public
profileRouter.get('/', profileCtrl.getProfiles);

// @route    POST api/profile
// @desc     Create or update user profile
// @access   Private
profileRouter.post(
  '/',
  authMiddleware,
  check('status', 'Validation Error : Status is required').notEmpty(),
  profileCtrl.createProfile
);

// @route    GET api/profile/me
// @desc     Get current users profile
// @access   Private
profileRouter.get('/me', authMiddleware, profileCtrl.currentProfile);

// @route    GET api/profile/user/:user_id
// @desc     Get profile by user ID
// @access   Public
profileRouter.get('/user/:user_id', profileCtrl.getProfileById);

// @route    DELETE api/profile
// @desc     Delete profile, user & posts
// @access   Private
profileRouter.delete('/', authMiddleware, profileCtrl.deleteProfile);

// @route    PUT api/profile/education
// @desc     Add profile education
// @access   Private
profileRouter.put(
  '/education',
  authMiddleware,
  check('school', 'School is required').notEmpty(),
  check('degree', 'Degree is required').notEmpty(),
  check('fieldofstudy', 'Field of study is required').notEmpty(),
  check('from', 'Please enter a date').notEmpty(),
  profileCtrl.addEducation
);

//delete education
// @route    DELETE api/profile/education/:edu_id
// @desc     Delete education from profile
// @access   Private

profileRouter.delete(
  '/education/:edu_id',
  authMiddleware,
  profileCtrl.deleteEducation
);

//Todo adding social
