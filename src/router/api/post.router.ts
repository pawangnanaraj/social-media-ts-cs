import express from 'express';
import { body, check } from 'express-validator';
import { authMiddleware } from '../../middleware/auth.middleware';
import postCtrl from '../../controllers/post.controller';
export const postRouter = express.Router();

// @route    POST api/posts
// @desc     Create a post
// @access   Private
postRouter.post(
  '/',
  authMiddleware,
  body('text', 'Text is required').notEmpty(),
  postCtrl.createPost
);

//route PUT /api/post/edit/:id
//edit a post
//access private
postRouter.put(
  '/edit/:id',
  authMiddleware,
  body('text', 'a text is required').notEmpty(),
  postCtrl.editPost
);

// @route    GET api/post
// @desc     Get all post
// @access   Private
postRouter.get('/', authMiddleware, postCtrl.getAllPost);

// @route    GET api/posts/:id
// @desc     Get post by ID
// @access   Private
postRouter.get('/:id', authMiddleware, postCtrl.getPostById);

// @route    DELETE api/posts/:id
// @desc     Delete a post
// @access   Private
postRouter.delete('/:id', authMiddleware, postCtrl.deletePost);

// @route    POST api/posts/comment/:postid
// @desc     Create a comment on a post
// @access   Private
postRouter.post(
  '/:postId',
  authMiddleware,
  check('text', 'Text is required').notEmpty(),
  postCtrl.createComment
);

//route PUT /api/comment/edit/:postId/:comment_id
//edit a comment
//access private

postRouter.put(
  '/edit/:postId/:comment_id',
  authMiddleware,
  check('text', 'a text is required').notEmpty(),
  postCtrl.editComment
);

// @route    DELETE api/posts/:id/:comment_id
// @desc     Delete comment
// @access   Private
postRouter.delete(
  '/:postId/:comment_id',
  authMiddleware,
  postCtrl.deleteComment
);

// @route    PUT api/posts/like/:id
// @desc     Like a post
// @access   Private
postRouter.put('/like/:postId', authMiddleware, postCtrl.likePost);

// @route    PUT api/posts/unlike/:id
// @desc     Unlike a post
// @access   Private
postRouter.put('/unlike/:postId', authMiddleware, postCtrl.unlikePost);
