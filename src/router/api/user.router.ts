import express from 'express';
import userCtrl from '../../controllers/user.controller';
import { body } from 'express-validator';

export const registerUserRouter = express.Router();

// route --> POST  api/users
//register user
registerUserRouter.post(
  '/',
  [
    body('name', 'Name is required').not().isEmpty(),
    body('email', 'Please enter a valid email').isEmail(),
    body('password', 'please enter a valid password with 6 values').isLength({
      min: 6,
    }),
  ],
  userCtrl.registerUser
);
