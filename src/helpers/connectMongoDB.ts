import mongoose from 'mongoose';
import config from 'config';
const db: string = config.get('mongoURI');

export const connectDB = async () => {
  try {
    await mongoose.connect(db);
    console.log('Connected to Database');
  } catch (error: any) {
    console.error(error.message);

    //exit the process with failure
    process.exit(1);
  }
};
