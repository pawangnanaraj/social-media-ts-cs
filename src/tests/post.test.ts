const request = require('supertest');
import express, { Application } from 'express';
import { postRouter } from '../router/api/post.router';

const app: Application = express();

describe('Testing post router', () => {
  describe('Testing create post /post', () => {
    it('without body', async () => {});
    it('without text', async () => {});
    it('without token', async () => {});
    it('successful', async () => {});
  });
});
