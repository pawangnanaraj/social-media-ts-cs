import { MongoMemoryServer } from 'mongodb-memory-server';
import mongoose from 'mongoose';

// beforeAll(runs once before all the tests)
let mongo: any;
beforeAll(async () => {
  mongo = await MongoMemoryServer.create();
  const mongoURI = mongo.getUri();
  await mongoose.connect(mongoURI);
  //jest.setTimeout(40000);
});

// //beforeEach(runs once before every test)
// beforeEach(async () => {
//   //jest.setTimeout(35000);
//   //   const collections = await mongoose.connection.db.collections();
//   //   for (let collection of collections) {
//   //     await collection.deleteMany({});
//   //   }
// });

//afterAll(runs after completion of all the tests)
afterAll(async () => {
  // jest.setTimeout(30000);
  await mongo.stop();
  await mongoose.connection.close();
});
