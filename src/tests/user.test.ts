const request = require('supertest');
import express, { Application } from 'express';
import { registerUserRouter } from '../router/api/user.router';

const app: Application = express();

app.use(express.json());
app.use('/api/users', registerUserRouter);

describe('Register User Route Testing : /users', () => {
  jest.setTimeout(300000);

  describe('POST /', () => {
    it('Without Username', async () => {
      const res = await request(app).post('/api/users').send({
        email: 'pawangr@gmail.com',
        password: '123456',
      });
      expect(res.statusCode).toEqual(400);
      expect(res.text).toContain('Name is required');
    });
    it('Without email', async () => {
      const res = await request(app).post('/api/users').send({
        name: 'Pawan GR',
        password: '123456',
      });
      expect(res.statusCode).toEqual(400);
      expect(res.text).toContain('Please enter a valid email');
    });
    it('Without password', async () => {
      const res = await request(app).post('/api/users').send({
        name: 'Pawan GR',
        email: 'pawangr@gmail.com',
      });
      expect(res.statusCode).toEqual(400);
      expect(res.text).toContain('please enter a valid password with 6 value');
    });

    it('Successfully register user', async () => {
      const res = await request(app)
        .post('/api/users')
        .set('Content-Type', 'application/json')
        .set('Accept', /json/)
        .send({
          name: 'Pawan GR',
          email: 'pawangr@gmail.com',
          password: '123456',
        });
      expect(res.statusCode).toEqual(200);
      expect(res.text).toBeTruthy();
    });

    it('With duplicate username', async () => {
      const res = await request(app)
        .post('/api/users')
        .set('Accept', /json/)
        .set('Content-Type', 'application/json')
        .send({
          name: 'Pawan GR',
          email: 'pawangr@gmail.com',
          password: '123456',
        });

      expect(res.text).toContain('user already exists');
      expect(res.statusCode).toEqual(400);
    });
  });
});
