const request = require('supertest');
import express, { Application } from 'express';
import { authRouter } from '../router/api/auth.router';
import { registerUserRouter } from '../router/api/user.router';

console.log(process.env.NODE_ENV);

const app: Application = express();

app.use(express.json());
app.use('/api/auth', authRouter);
app.use('/api/users', registerUserRouter);

//initialising a user
const user = {
  name: 'Jeswin GR',
  email: 'jeswingr@gmail.com',
  password: '123456',
};

let token: any;
//-------------------------------Testing Login ----------------------------------
describe('Auth Router /api/auth', () => {
  jest.setTimeout(800000);
  beforeAll(async () => {
    token = await request(app)
      .post('/api/users')
      .set('Content-Type', 'application/json')
      .set('Accept', /json/)
      .send(user);
  });

  describe('Testing login', () => {
    it('Invalid password', async () => {
      const res = await request(app)
        .post('/api/auth')
        .set('Content-Type', 'application/json')
        .set('Accept', /json/)
        .send({
          email: 'jeswingr@gmail.com',
          password: '12345',
        });
      expect(res.statusCode).toEqual(401);
      expect(res.text).toContain('Invalid Password');
    });

    it('Invalid Email', async () => {
      const res = await request(app)
        .post('/api/auth')
        .set('Content-Type', 'application/json')
        .set('Accept', /json/)
        .send({
          email: 'jeswing@gmail.com',
          password: '123456',
        });
      expect(res.statusCode).toEqual(401);
      expect(res.text).toContain('User not Found (or) Invalid email');
    });

    it('Successful login', async () => {
      const res = await request(app)
        .post('/api/auth')
        .set('Content-Type', 'application/json')
        .set('Accept', /json/)
        .send({
          email: 'jeswingr@gmail.com',
          password: '123456',
        });
      expect(res.statusCode).toEqual(200);
      expect(res.text).toBeTruthy();
    });
  });

  //---------------------------Testing Authentication ---------------------------------
  describe('Testing getting Auth token', () => {
    it('Without Token', async () => {
      const res = await request(app).get('/api/auth');
      expect(res.statusCode).toEqual(401);
      expect(res.text).toContain('Unauthorized');
    });

    it('Incorrect Token', async () => {
      const res = await request(app)
        .get('/api/auth')
        .set('x-auth-token', 'asdfdf1322');
      expect(res.statusCode).toEqual(401);
      expect(res.text).toBe('Unauthorized');
    });

    it('Correct Token', async () => {
      // console.log(token);
      const res = await request(app)
        .get('/api/auth')
        .set('x-auth-token', token.body.token);
      expect(res.statusCode).toEqual(200);
      expect(res.body.name).toBe(user.name);
    });
  });
});
