import { check } from 'express-validator';

export const loginValidator = () => {
  return [
    check('email').isEmail().withMessage('email is required'),
    check('password').notEmpty().withMessage('password is required'),
  ];
};
