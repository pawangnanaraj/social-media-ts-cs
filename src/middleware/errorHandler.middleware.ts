import httpStatusCodes from './errorCode.middleware';

export class NotFoundError extends Error {
  constructor(
    public message: string,
    public status = httpStatusCodes.NOT_FOUND
  ) {
    super(message);
    this.name = 'notFoundError';
  }
}

export class NotMatchError extends Error {
  constructor(
    public message: string,
    public status = httpStatusCodes.UNAUTHORISED_USER
  ) {
    super(message);
    this.name = 'NotMatchError';
  }
}

export class UnauthorizedUserError extends Error {
  constructor(
    public message: string,
    public status = httpStatusCodes.UNAUTHORISED_USER
  ) {
    super(message);
    this.name = 'unauthorizedUserError';
  }
}

export class AlreadyLikedError extends Error {
  constructor(
    public message: string,
    public status = httpStatusCodes.BAD_REQUEST
  ) {
    super(message);
    this.name = 'alreadyLikedError';
  }
}

export class UserAlreadyAvailableError extends Error {
  constructor(
    public message: string,
    public status = httpStatusCodes.BAD_REQUEST
  ) {
    super(message);
    this.name = 'UserAlreadyAvailableError';
  }
}
